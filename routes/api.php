<?php

use App\Http\Controllers\AdminController;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/admin/1/schedules/1', function () {
    return Appointment::create([
        'doctor_id' =>1,
        'admins_id' =>1,
        'start_time'=>'12:12:12',
        'end_time'=>'1:1:1',
        'date'=>'2023-1-1',
        'patient_id'=>1
    ]);
});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
