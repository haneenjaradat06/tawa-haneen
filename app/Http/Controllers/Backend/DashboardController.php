<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctor;

class DashboardController extends Controller
{
    //

    public function index(){
        $doctors = Doctor::all();
        return view('backend.pages.dashboard.index');
    }

    public function show(){
        
    }

    public function create(){
        
    }

    public function store(){
        
    }

    public function edit(){
        
    }

    public function update(){
        
    }

    public function trash(){
        
    }

    public function restore(){
        
    }

    public function forceDelete(){
        
    }

   
}
