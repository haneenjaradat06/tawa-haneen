<?php

namespace App\Http\Controllers;

// use Barryvdh\DomPDF\PDF;

use App\Models\Report;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class PdfController extends Controller
{
   public function  index(){
    $reports= Report::all();
    return view('Pdf.view',compact('reports'));
    }
     public function viewPdf(){
         $reports= Report::all();
         $pdf=PDF::loadView('Pdf.view',array('reports'=>$reports))->setPaper('a1');
         return $pdf->stream();

        // $pdf= PDF::loadHTML('<h1> Hello Nawar </h1>');
        // return $pdf->stream();

}
}
